package com.company;
import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Main {
    public static void main(String[] args) {
	    int width = 1024;
	    int height = 768;
	    int canvasWidth = 400;
	    int canvasHeight = 400;

        JFrame j = new JFrame();

        // Create canvas, add to frame
        MyCanvas m = new MyCanvas();
        m.setBounds(width/2-(canvasWidth/2), height/2-(canvasHeight/2), canvasWidth, canvasHeight);
        m.setBorder(new CompoundBorder(new EmptyBorder(0, 0, 0, 0), new LineBorder(Color.BLACK)));
        j.add(m);
        JButton forward = new JButton("^");
        forward.setBounds(width/4, height-90, width/2, 20);
        forward.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //your actions
                m.move(0, 0, 1);
                m.paintComponent(m.getGraphics());
            }
        });


        JButton backward = new JButton("V");
        backward.setBounds(width/4, height-60, width/2, 20);
        backward.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                m.move(0, 0, -1);
                m.paintComponent(m.getGraphics());
            }
        });


        JButton yawLeft = new JButton("<");
        yawLeft.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                m.rotate(0, 0, -0.01);
                m.paintComponent(m.getGraphics());
            }
        });
        yawLeft.setBounds(width/4, height-30, width/2, 20);

        JButton yawRight = new JButton(">");
        yawRight.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                m.rotate(0, 0, 0.01);
                m.paintComponent(m.getGraphics());
            }
        });
        yawRight.setBounds(width/4, height, width/2, 20);

        j.add(forward);
        j.add(backward);
        j.add(yawLeft);
        j.add(yawRight);
        // Set frame size
        j.setSize(width, height);
        j.setLayout(null);
        j.setVisible(true);
//
//        Matrix m = new Matrix(new double[][]{{10}, {1}});
//        Matrix n = new Matrix(1, 2);
//        Matrix result = m.multiply(n);
//        for (int i = 0; i < result.getRows(); i++) {
//            System.out.println((int) result.getCell(i, 0));
//        }
        System.out.println("Hello world");
    }
}
