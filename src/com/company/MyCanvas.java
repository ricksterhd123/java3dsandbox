package com.company;
import javax.swing.*;
import java.awt.*;

public class MyCanvas extends JPanel {
    private Vector2 click;
    private final Camera cam;
    public MyCanvas()
    {
        cam = new Camera(new TaitBryanAngles(0, 0, 0), new Vector3(0, 0, 0), 10);
    }

    public void move(double x, double y, double z) {
        this.cam.setPosition(new Vector3(this.cam.getPosition().getX() + x, this.cam.getPosition().getY() + y, this.cam.getPosition().getZ() + z));
    }

    public void rotate(double pitch, double yaw, double roll)
    {
        this.cam.setRotation(new TaitBryanAngles(this.cam.getRotation().getPitch() + pitch, this.cam.getRotation().getYaw() + yaw, this.cam.getRotation().getRoll() + roll));
    }

    public void paintComponent(Graphics g)
    {
        System.out.println("Drawn");
        super.paintComponent(g);
        super.paintBorder(g);
        Cube cube = new Cube(0, 0, 1, 100, 100, 2);
        Vector3[] vertices = cube.getVertices();
        for (int i = 0; i < vertices.length; i++) {
            Vector2 projection = this.cam.perspectiveProjection(vertices[i].getX(), vertices[i].getY(), vertices[i].getZ());
            g.setColor(new Color(255, 0, 0));
            g.fillRect((int) projection.getX() + (int) this.getBounds().getWidth()/2, (int) projection.getY() + (int) this.getBounds().getHeight()/2, 3, 3);
        }
        //g.drawRect((int) this.getBounds().getWidth()/2, (int) this.getBounds().getHeight()/2, 1, 1);
    }

}