package com.company;
public class Matrix {
    private int rows;
    private int cols;
    private double [][] matrix; // hopefully

    /**
     * Creates an identity matrix of dimension rows * cols
     * @param rows number of rows
     * @param cols number of columns
     */
    Matrix(int rows, int cols) {
        this.matrix = new double[rows][cols];
        this.rows = rows;
        this.cols = cols;

        // Identity matrix
        for (int row = 0; row < this.rows; row++) {
            for (int col = 0; col < this.cols; col++) {
                this.matrix[row][col] = (row == col)? 1 : 0;
            }
        }


    }

    /**
     * Creates the matrix with 2D array of dimension [row][column];
     * we assume number of columns is the same in each row otherwise you may get IndexOutofbounds
     * @param rowColumns - 2D array [row]x[column]
     */
    Matrix(double[][] rowColumns) {
        this.matrix = rowColumns;
        this.rows = rowColumns.length;
        this.cols = rowColumns[0].length;   // Assume number of columns same for each row
    }

    void setCell(int row, int col, double value) {
        this.matrix[row][col] = value;
    }

    double getCell(int row, int col) {
        return this.matrix[row][col];
    }

    int getRows() {
        return this.rows;
    }

    int getCols() {
        return this.cols;
    }

    /**
     * Multiplies this matrix with the matrix m, returns matrix result
     * @param m The matrix to multiply with this.
     * @return Matrix result
     */
    Matrix multiply(Matrix m) throws Error{
        int aRows = this.rows;
        int aCols = this.cols;
        int bRows = m.getRows();
        int bCols = m.getCols();

        if (aCols != bRows)
            throw new Error("Error: multiplication ["+aRows+" x "+aCols+"] with ["+bRows+" x "+bCols+"] is undefined.");

        Matrix result = new Matrix(aRows, bCols);
        for (int row = 0; row < aRows; row++) {
            for (int column = 0; column < bCols; column++) {
                int sum = 0;
                for (int n = 0; n < aCols; n++){
                    sum += this.getCell(row, n) * m.getCell(n, column);
                }
                result.setCell(row, column, sum);
            }
        }
        return result;
    }

    Matrix multiply(double scalar) {
        Matrix result = new Matrix(this.rows, this.cols);
        for (int i = 0; i < this.rows; i++) {
            for (int j = 0; j < this.cols; j++) {
                result.setCell(i, j, this.getCell(i, j) * scalar);
            }
        }
        return result;
    }

    Matrix add(Matrix m) throws Error {
        if (this.rows != m.getRows() || this.cols != m.getCols())
            throw new Error("Error: addition ["+this.rows+" x "+this.cols+"] with ["+m.getRows()+" x "+m.getCols()+"] is undefined.");

        Matrix result = new Matrix(m.rows, m.cols);
        for (int i = 0; i < m.rows; i++) {
            for (int j = 0; j < m.cols; j++) {
                result.setCell(i, j, this.getCell(i, j) + m.getCell(i, j));
            }
        }
        return result;
    }

    Matrix sub(Matrix m) throws Error {
        if (this.rows != m.getRows() || this.cols != m.getCols())
            throw new Error("Error: subtraction ["+this.rows+" x "+this.cols+"] with ["+m.getRows()+" x "+m.getCols()+"] is undefined.");

        Matrix result = new Matrix(m.rows, m.cols);
        for (int i = 0; i < m.getRows(); i++) {
            for (int j = 0; j < m.getCols(); j++) {
                result.setCell(i, j, this.getCell(i, j) - m.getCell(i, j));
            }
        }
        return result;
    }

    Matrix transpose() {
        Matrix result = new Matrix(this.getCols(), this.getRows());
        for (int i = 0; i < result.getRows(); i++) {
            for (int j = 0; j < result.getCols(); j++) {
                result.setCell(i, j, this.getCell(j, i));
            }
        }
        return result;
    }
}
