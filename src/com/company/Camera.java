package com.company;

public class Camera {
    private TaitBryanAngles rotation;
    private Vector3 position;
    private Vector3 displayPosition;

    Camera (double focalLength) {
        this.displayPosition = new Vector3(0, 0, focalLength);
        this.rotation = new TaitBryanAngles();
        this.position = new Vector3();
    }

    Camera (TaitBryanAngles rotation, Vector3 position, double focalLength) {
        this.rotation = rotation;
        this.position = position;
        this.displayPosition = new Vector3(0, 0, focalLength);
    }

    void setRotation(TaitBryanAngles rotation) {
        this.rotation = rotation;
    }

    TaitBryanAngles getRotation() {
        return this.rotation;
    }

    void setPosition(Vector3 position) {
        this.position = position;
    }

    Vector3 getPosition() {
        return this.position;
    }

    /**
     * Perform perspective projection
     * @param x - The x position of the vertex to project
     * @param y - The y position of the vertex to project
     * @param z - The z position of the vertex to project
     * @return Vector2 relative coordinates [-1, -1] => bottom left corner of screen, [1, 1] => top right corner of screen
     */
    Vector2 perspectiveProjection(double x, double y, double z)
    {
        Matrix f = new Matrix(new double[][] {{1, 0, this.displayPosition.getX()/this.displayPosition.getZ()}, {0, 1, this.displayPosition.getY()/this.displayPosition.getZ()}, {0, 0, 1/this.displayPosition.getZ()}}).multiply(this.rotation.toMatrix().transpose().multiply(new Matrix(new double[][] { {x-position.getX()}, {y-position.getY()}, {z-position.getZ()} })));
        return new Vector2(f.getCell(0, 0) / f.getCell(2, 0), f.getCell(1, 0) / f.getCell(2, 0));
    }
}
