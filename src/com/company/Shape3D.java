package com.company;

public class Shape3D {
    private Vector3[] vertices;

    Shape3D(int noVertices) {
        this.vertices = new Vector3[noVertices];
    }

    Shape3D(Vector3[] vertices) {
        this.vertices = vertices;
    }

    Vector3[] getVertices() {
        return this.vertices;
    }

    void setVertices(Vector3[] vertices) {
        this.vertices = vertices;
    }
}
