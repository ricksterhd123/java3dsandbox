package com.company;

public class Vector3 {
    private double x;
    private double y;
    private double z;

    Vector3() {
        this(0,0,0);
    }

    Vector3(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    double getX()
    {
        return this.x;
    }

    void setX(double x)
    {
        this.x = x;
    }

    double getY()
    {
        return this.y;
    }

    void setY(double y)
    {
        this.y = y;
    }

    double getZ()
    {
        return this.z;
    }

    void setZ(double z)
    {
        this.z = z;
    }

    Vector3 add(Vector3 b)
    {
        return new Vector3(this.x+b.getX(), this.y+b.getY(), this.z+b.getZ());
    }

    Vector3 sub(Vector3 b)
    {
        return new Vector3(this.x-b.getX(), this.y-b.getY(), this.z-b.getZ());
    }

    Vector3 multiply(double scalar)
    {
        return new Vector3(this.x*scalar, this.y*scalar, this.z*scalar);
    }

    double dot(Vector3 b)
    {
        return this.x * b.getX() + this.y * b.getY() + this.z * b.getZ();
    }

    Vector3 cross(Vector3 b)
    {
        return new Vector3(this.y*b.getZ() - this.z*b.getY(),this.z*b.getX() - this.x*b.getZ(), this.x*b.getY() - this.y*b.getX());
    }
}
