package com.company;

public class Vector2 {
    private double x;
    private double y;

    Vector2() {
        this.x = 0;
        this.y = 0;
    }

    Vector2(double x, double y) {
        this.x = x;
        this.y = y;
    }

    double getX()
    {
        return this.x;
    }

    void setX(int x) {
        this.x = x;
    }

    double getY()
    {
        return this.y;
    }

    void setY(int y) {
        this.y = y;
    }
    Vector2 add(Vector2 b) {
        return new Vector2(this.x + b.getX(), this.y + b.getY());
    }

    Vector2 sub(Vector2 b) {
        return new Vector2(this.x - b.getX(), this.y - b.getY());
    }

    Vector2 multiply(double scalar) {
        return new Vector2( this.x * scalar, this.y * scalar);
    }
}
