package com.company;

import java.awt.*;

public class Cube extends Shape3D {
    Cube(double x, double y, double z, double width, double height, double length)
    {
        super(8);
        this.setVertices(new Vector3[]{
                new Vector3(x, y, z),
                new Vector3(x+width, y, z),
                new Vector3(x, y+height, z),
                new Vector3(x+width, y+height, z),
                new Vector3(x, y, z+length),
                new Vector3(x+width, y, z+length),
                new Vector3(x, y + height, z + length),
                new Vector3(x+width, y + height, z+length)
        });
    }
}
