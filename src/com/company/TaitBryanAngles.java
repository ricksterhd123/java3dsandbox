package com.company;
import java.math.*;

public class TaitBryanAngles {
    private double pitch; // x
    private double yaw;   // y
    private double roll;  // z

    TaitBryanAngles() {
        this.pitch =0;
        this.yaw   =0;
        this.roll  =0;
    }

    TaitBryanAngles(double pitch, double yaw, double roll) {
        this.pitch = pitch;
        this.yaw = yaw;
        this.roll = roll;
    }
    double getPitch() {
        return this.pitch;
    }

    void setPitch(double pitch) {
        this.pitch = pitch;
    }

    double getRoll() {
        return this.roll;
    }

    void setRoll(double roll) {
        this.roll = roll;
    }

    double getYaw() {
        return this.yaw;
    }

    void setYaw(double yaw) {
        this.yaw = yaw;
    }

    /**
     * Converts Tait-Bryan angles to a 3x3 rotation matrix X(pitch), Y(yaw), Z(roll)
     * can't remember why i implemented this...
     * @return the rotation matrix
     */
    Matrix toMatrix() {
        return new Matrix(new double[][] {
                {Math.cos(yaw)*Math.cos(roll), -Math.cos(yaw)*Math.sin(roll), Math.sin(yaw)},
                {Math.cos(pitch)*Math.sin(roll) + Math.cos(roll)*Math.sin(pitch)*Math.sin(yaw), Math.cos(pitch) * Math.cos(roll) - Math.sin(pitch)*Math.sin(yaw)*Math.sin(roll), -Math.cos(yaw)*Math.sin(pitch)},
                {Math.sin(pitch)*Math.sin(roll) - Math.cos(pitch)*Math.cos(roll)*Math.sin(yaw), Math.cos(roll)*Math.sin(pitch) + Math.cos(pitch)*Math.sin(yaw)*Math.sin(roll), Math.cos(pitch)*Math.cos(yaw)}
        });
    }
}
